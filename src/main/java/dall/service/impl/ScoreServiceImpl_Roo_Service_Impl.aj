// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package dall.service.impl;

import dall.model.Score;
import dall.repository.ScoreRepository;
import dall.service.impl.ScoreServiceImpl;
import io.springlets.data.domain.GlobalSearch;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

privileged aspect ScoreServiceImpl_Roo_Service_Impl {
    
    declare @type: ScoreServiceImpl: @Service;
    
    declare @type: ScoreServiceImpl: @Transactional(readOnly = true);
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private ScoreRepository ScoreServiceImpl.scoreRepository;
    
    /**
     * TODO Auto-generated constructor documentation
     * 
     * @param scoreRepository
     */
    @Autowired
    public ScoreServiceImpl.new(ScoreRepository scoreRepository) {
        setScoreRepository(scoreRepository);
    }

    /**
     * TODO Auto-generated method documentation
     * 
     * @return ScoreRepository
     */
    public ScoreRepository ScoreServiceImpl.getScoreRepository() {
        return scoreRepository;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param scoreRepository
     */
    public void ScoreServiceImpl.setScoreRepository(ScoreRepository scoreRepository) {
        this.scoreRepository = scoreRepository;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param score
     */
    @Transactional
    public void ScoreServiceImpl.delete(Score score) {
        getScoreRepository().delete(score);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param entities
     * @return List
     */
    @Transactional
    public List<Score> ScoreServiceImpl.save(Iterable<Score> entities) {
        return getScoreRepository().save(entities);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param ids
     */
    @Transactional
    public void ScoreServiceImpl.delete(Iterable<Long> ids) {
        List<Score> toDelete = getScoreRepository().findAll(ids);
        getScoreRepository().deleteInBatch(toDelete);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param entity
     * @return Score
     */
    @Transactional
    public Score ScoreServiceImpl.save(Score entity) {
        return getScoreRepository().save(entity);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param id
     * @return Score
     */
    public Score ScoreServiceImpl.findOne(Long id) {
        return getScoreRepository().findOne(id);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param id
     * @return Score
     */
    public Score ScoreServiceImpl.findOneForUpdate(Long id) {
        return getScoreRepository().findOneDetached(id);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param ids
     * @return List
     */
    public List<Score> ScoreServiceImpl.findAll(Iterable<Long> ids) {
        return getScoreRepository().findAll(ids);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return List
     */
    public List<Score> ScoreServiceImpl.findAll() {
        return getScoreRepository().findAll();
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return Long
     */
    public long ScoreServiceImpl.count() {
        return getScoreRepository().count();
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Score> ScoreServiceImpl.findAll(GlobalSearch globalSearch, Pageable pageable) {
        return getScoreRepository().findAll(globalSearch, pageable);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param ids
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Score> ScoreServiceImpl.findAllByIdsIn(List<Long> ids, GlobalSearch globalSearch, Pageable pageable) {
        return getScoreRepository().findAllByIdsIn(ids, globalSearch, pageable);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return Class
     */
    public Class<Score> ScoreServiceImpl.getEntityType() {
        return Score.class;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return Class
     */
    public Class<Long> ScoreServiceImpl.getIdType() {
        return Long.class;
    }
    
}
