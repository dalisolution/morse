package dall.repository;

import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import dall.model.Score;

/**
 * = ScoreRepositoryImpl
 *
 * TODO Auto-generated class documentation
 *
 */ 
@RooJpaRepositoryCustomImpl(repository = ScoreRepositoryCustom.class)
public class ScoreRepositoryImpl extends QueryDslRepositorySupportExt<Score> {

    /**
     * TODO Auto-generated constructor documentation
     */
    ScoreRepositoryImpl() {
        super(Score.class);
    }
}