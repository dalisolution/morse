package dall.repository;
import dall.model.Score;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustom;

/**
 * = ScoreRepositoryCustom
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustom(entity = Score.class)
public interface ScoreRepositoryCustom {
}
