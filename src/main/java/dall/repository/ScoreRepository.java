package dall.repository;
import dall.model.Score;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;

/**
 * = ScoreRepository
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = Score.class)
public interface ScoreRepository {
}
