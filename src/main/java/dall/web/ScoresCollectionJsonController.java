package dall.web;
import dall.model.Score;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;

/**
 * = ScoresCollectionJsonController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = Score.class, type = ControllerType.COLLECTION)
@RooJSON
public class ScoresCollectionJsonController {
}
