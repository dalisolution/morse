package dall.web;
import dall.model.Score;
import dall.service.api.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;

/**
 * = ScoreDeserializer
 TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = Score.class)
public class ScoreDeserializer extends JsonObjectDeserializer<Score> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ScoreService scoreService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param scoreService
     * @param conversionService
     */
    @Autowired
    public ScoreDeserializer(@Lazy ScoreService scoreService, ConversionService conversionService) {
        this.scoreService = scoreService;
        this.conversionService = conversionService;
    }
}
