package dall.web;
import dall.model.Score;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;

/**
 * = ScoresItemJsonController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = Score.class, type = ControllerType.ITEM)
@RooJSON
public class ScoresItemJsonController {
}
