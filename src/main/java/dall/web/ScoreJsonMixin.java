package dall.web;
import dall.model.Score;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;

/**
 * = ScoreJsonMixin
 TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = Score.class)
public abstract class ScoreJsonMixin {
}
