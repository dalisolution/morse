/**
 * Morse alphabet
 * 
 * @author Dalibor Dyba 
 */

	var morseAlphabet = new Map();
      		morseAlphabet.set('a','01');
      		morseAlphabet.set('b','1000');
      		morseAlphabet.set('c','1010');
      		morseAlphabet.set('d','100');
      		morseAlphabet.set('e','.');
      		morseAlphabet.set('f','0010');
      		morseAlphabet.set('g','110');
      		morseAlphabet.set('h','0000');
      		morseAlphabet.set('i','00');
      		morseAlphabet.set('j','0111');
      		morseAlphabet.set('k','101');
      		morseAlphabet.set('l','0100');
      		morseAlphabet.set('m','11');
      		morseAlphabet.set('n','10');
      		morseAlphabet.set('o','111');
      		morseAlphabet.set('p','0110');
      		morseAlphabet.set('q','1101');
      		morseAlphabet.set('r','010');
      		morseAlphabet.set('s','000');
      		morseAlphabet.set('t','1');
      		morseAlphabet.set('u','001');
      		morseAlphabet.set('v','0001');
      		morseAlphabet.set('w','011');
      		morseAlphabet.set('x','1001');
      		morseAlphabet.set('y','1011');
      		morseAlphabet.set('z','1100');

      		morseAlphabet.set('1','01111');
      		morseAlphabet.set('2','00111');
      		morseAlphabet.set('3','00011');
      		morseAlphabet.set('4','00001');
      		morseAlphabet.set('5','00000');
      		morseAlphabet.set('6','10000');
      		morseAlphabet.set('7','11000');
      		morseAlphabet.set('8','11100');
      		morseAlphabet.set('9','11110');
      		morseAlphabet.set('10','11111');
      		
      		function translateBinary(code){
      				var character=null;
					var entries = Array.from(morseAlphabet.entries());
					
					entries.some(function (entry, index) {
						if(entry[1] == code){
							character = entry[0];
							return true;
						}
					});
					
					return character;
      		}
      		
